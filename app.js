// require module
const express = require('express')
const app = express()
require('dotenv').config();
const port = process.env.PORT;

// import data dan filterdata
const data = require("./data.js");
const {byageAndBanana, byAgeAndFemale, byEyeAndAge, byCompanyAndEye, 
        byRegistAndActive} = require("./filterData.js");

// Static Files
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/img', express.static(__dirname + 'public/img'))
// Set Views
app.set('views', './views')

// set view engine menggunakan EJS 
app.set('view engine', 'ejs')

// fungsi untuk mengecek apakah result kosong atau tidak
function resHandler(value){
  if (!value.length){
    // jika resultnya kosong maka akan menampilkan message ini
    const err = { message : "data tidak ada/tidak ditemukan"}
    return err
  }else {
    // jika data tidak kosong maka akan di tampilkan total data dan datanya
    let dataJson = {
          "totalData": value.length,
          "allData": value
        };
    return dataJson
  }
}

app.get('/', (req, res) => {
  res.render('index')
})

app.get('/about', (req, res) => {
  res.render('about')
})

app.get('/tabelData/:filter', (req, res) => {
  //  const value = req.params.sort
  if(req.params.filter === "all"){
    filter = data
  }else if (req.params.filter === "pemudapisang"){
    filter = byageAndBanana
  }else if (req.params.filter === "karyawanfsw"){
    filter = byAgeAndFemale
  }else if (req.params.filter === "matabiru"){
    filter = byEyeAndAge
  }else if (req.params.filter === "intelhijau"){
    filter = byCompanyAndEye
  }else if (req.params.filter === "veteran"){
    filter = byRegistAndActive
  }else {
    filter = []
  }
  res.render('tabelData',{
    data, 
    filter
  })
})

app.get('/data', (req, res) => {
  res.type('application/json')
  res.json(data)
})

app.get('/data1', (req, res) => {
  res.type('application/json')
  res.json(resHandler(byageAndBanana))
})

app.get('/data2', (req, res) => {
  res.type('application/json')
  res.json(resHandler(byAgeAndFemale))
})

app.get('/data3', (req, res) => {
  res.type('application/json')
  res.json(resHandler(byEyeAndAge))
})

app.get('/data4', (req, res) => {
  res.type('application/json')
  res.json(resHandler(byCompanyAndEye))
})

app.get('/data5', (req, res) => {
  res.type('application/json')
  res.json(resHandler(byRegistAndActive))
})

// jika halaman tidak ada atau tidak di temukan 
// akan menampilkan halaman 404
app.use('/', (req, res) => {
  res.status(404)
  res.render('404')
})

app.listen(port, () => {
  console.log(`Server sudah berjalan, silahkan buka di http://localhost:${port}`)
})